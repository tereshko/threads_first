#include <iostream>
#include <cstdlib>
#include <pthread.h>

struct FORMY {
	int argument;
	};

void* thread_func(void *arg) {
	int valueFromStruct;
	
	FORMY* formu;
	formu = static_cast<FORMY*>(arg);
	
	valueFromStruct = formu->argument;
	
	delete formu;
	
	std::cout << "------------------------------" << std::endl;
	std::cout << "thread with number: " << valueFromStruct << std::endl;
	for (int i = 0; i <= 2; i++) {
		std::cout << "answer: " << valueFromStruct + i << std::endl;
	}
	std::cout << "------------------------------" << std::endl;
}

int main( int argc, char * argv[]) {
	pthread_t threada, threadb, threadc;
	
	FORMY* valueForThreada = new FORMY();
	valueForThreada->argument = 10;
	std::cout << "thread 1: " << valueForThreada->argument << std::endl;
	pthread_create( &threada, NULL, thread_func, valueForThreada);

	FORMY* valueForThreadb = new FORMY();
	valueForThreadb->argument = 100;
	
	std::cout << "thread 2: " << valueForThreadb->argument << std::endl;
	pthread_create( &threadb, NULL, thread_func, valueForThreadb);
	
	FORMY* valueForThreadc = new FORMY();
	valueForThreadc->argument = 1000;
	
	std::cout << "thread 3: " << valueForThreadc->argument << std::endl;
	pthread_create( &threadc, NULL, thread_func, valueForThreadc);
	
	pthread_join(threada, NULL);
	pthread_join(threadb, NULL);
	pthread_join(threadc, NULL);
	return 0;
}
